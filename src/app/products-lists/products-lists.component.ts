import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products-lists',
  templateUrl: './products-lists.component.html',
  styleUrls: ['./products-lists.component.css']
})
export class ProductsListsComponent implements OnInit {
  productsList: any;
  car: any = [];
  carAmount = 0;
  apiUrl = "https://8o7ka9777j.execute-api.us-east-1.amazonaws.com/dev";
 // apiUrl = "http://localhost:5000";
  constructor(private http: HttpClient) { }

  async ngOnInit(): Promise<void> {
    this.productsList = await this.http
    .get<any>(this.apiUrl + "/products?skip=0&limit=5")
    .toPromise();
    console.log(this.productsList)
  }

  async makeTransaction(){
    let headers = { 'content-type': 'application/json'} 
    const transactionMaked = await this.http
    .post<any>(this.apiUrl + "/transactions", {
      productIds: this.car
    }, 
    {'headers':headers})
    .toPromise();
    if(transactionMaked.status == "Created"){
      window.location.replace(transactionMaked.redirectUrl);
    }
    else{
      alert("Error al generar la transaccion, intentalo mas tarde!")
    }
    console.log(transactionMaked)
  }

  calculateroral(){
    this.carAmount = 0;
    this.car.forEach(currentItem => {
    const currentProduct = this.productsList.products.find(curitem => curitem._id.$oid == currentItem);
    console.log(currentProduct)
    this.carAmount = this.carAmount + currentProduct.numbersPrice
    });
  }
  addToCar(item: any){
    const isAlready = this.car.find(curitem => curitem == item._id.$oid);
    if(!isAlready)
      this.car.push(item._id.$oid);
      console.log(this.car);
    this.calculateroral();
  }

  removeFromCar(item: any){
    const newCar = this.car.filter(curitem => curitem != item._id.$oid);
    this.car = newCar;

    console.log(this.car);
    this.calculateroral();
  }


}
