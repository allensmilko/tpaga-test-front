import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsListsComponent } from './products-lists/products-lists.component';

const routes: Routes = [
  {
    "path": "",
    "component": ProductsListsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
